const data = [
  {
    'folder': true,
    'title': 'Grow',
    'children': [
      {
        'title': 'logo.png'
      },
      {
        'folder': true,
        'title': 'English',
        'children': [
          {
            'title': 'Present_Perfect.txt'
          }
        ]
      }
    ]
  },
  {
    'folder': true,
    'title': 'Soft',
    'children': [
      {
        'folder': true,
        'title': 'NVIDIA',
        'children': null
      },
      {
        'title': 'nvm-setup.exe'
      },
      {
        'title': 'node.exe'
      }
    ]
  },
  {
    'folder': true,
    'title': 'Doc',
    'children': [
      {
        'title': 'project_info.txt'
      }
    ]
  },
  {
    'title': 'credentials.txt'
  }
];

const rootNode = document.getElementById('root');


function buildtree (elements, parentNode) {
  let fileList = document.createElement('ul');

  for (let element of elements) {
    let listItem = document.createElement('li');
    let titleParagraph = document.createElement('p');
    let nodeText = document.createTextNode(element.title);
    let materialIcon = document.createElement('i');

    materialIcon.classList.add('material-icons')
    titleParagraph.classList.add('element');
    titleParagraph.appendChild(materialIcon);
    titleParagraph.appendChild(nodeText);
    listItem.appendChild(titleParagraph);
    fileList.appendChild(listItem);
    parentNode.appendChild(fileList);

    if (element.folder) {
      listItem.classList.add('folder');
      materialIcon.appendChild(document.createTextNode('folder'));
      materialIcon.classList.add('icon-folder');

      if (element.children) {
        buildtree(element.children, listItem);
      } else {
        let emptyFolderParagraph = document.createElement('p');
        let emptyTextNode = document.createTextNode('Folder is empty');

        emptyFolderParagraph.classList.add('empty-folder');
        emptyFolderParagraph.appendChild(emptyTextNode);
        listItem.appendChild(emptyFolderParagraph);
      }

      titleParagraph.addEventListener('click', function(e) {
        e.target.parentNode.classList.toggle('remove');

        if (!e.target.parentNode.classList.contains('remove')) {
          e.target.children[0].textContent = 'folder';
        } else {
          e.target.children[0].textContent = 'folder_open';
        }
      })

    } else {
      materialIcon.appendChild(document.createTextNode('insert_drive_file'));
      materialIcon.classList.add('icon-file');
    }
  }
}

buildtree(data, rootNode);
