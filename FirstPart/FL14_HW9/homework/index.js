let two = 2;
let five = 5;

// 1
function convert(...arr){
  let array = new Array();
  array.push(...arr);
  for (let i = 0; i < array.length; i++) {
    if (typeof array[i] === 'string') {
      array[i] = parseInt(array[i]);
    } else if (typeof array[i] === 'number') {
      array[i] += '';
    }
  }
  console.log(array)
}

// 2
function executeforEach(arr, callback) {
    for (let i = 0; i < arr.length; i++) {
        callback(arr[i]);
    }
}



// 3
function mapArray(arr, callback) {
    let newArr = [];
    executeforEach(arr, function (el) {
        newArr.push(callback(parseInt(el)));
    })
}



// 4
function filterArray(arr, callback) {
    let newArr = [];
    executeforEach(arr, function (el) {
      return callback(el) ? newArr.push(el) : 0;
    })
    return newArr;
}


// 5
function getValuePosition(arr, value){
  for (let i = 0; i < arr.length+1; i++) {
    if (arr[i] === value) {
      return console.log(i+1);
    }
  } 
    return console.log(false);
}


// 6
function flipOver(string){
  let newStr = '';
  for (let i = string.length - 1; i >= 0; i--) {
          newStr += string[i];
      }
  console.log(newStr);
  }



// 7
function makeListFromRange(arr){
  let array = [];
  for (let i = arr[0]; i <= arr[1]; i++) {
    array.push(i);
  }
  console.log(array)
  }


// 8
function getArrayOfKeys(objects, property) {
    let resultList = [];

    executeforEach(objects, value => {
        resultList.push(value[property]);
    });

    return resultList;
}

// 9
function getTotalWeight(objects){
    let result = 0;
    for (let i = 0; i < objects.length; i++) {
        result += objects[i].weight;
    }
    return result;
}

// 10
const date = new Date(2020, 0, 2);

function getPastDay(value, days){
  let dateCopy = new Date(value);

dateCopy.setDate(value.getDate() - days);
return console.log(dateCopy.getDate());
}



// 11
function formatDate(date){
  let year = date.getFullYear().toString();
  let month = (date.getMonth()+1).toString().length===two?
  (date.getMonth()+1).toString():'0'+(date.getMonth()+1).toString();
  let day = date.getDate().toString().length===two?date.getDate().toString():'0'+date.getDate().toString();
  let hours = date.getHours().toString().length===two?date.getHours().toString():'0'+date.getHours().toString();
  let minutes = (parseInt(date.getMinutes()/five)*five).toString().length===two?
  (parseInt(date.getMinutes()/five)*five).toString():'0'+(parseInt(date.getMinutes()/five)*five).toString();
  let newDate = year+'/'+month+'/'+day+' '+hours+':'+minutes;
  return console.log(newDate);
}
