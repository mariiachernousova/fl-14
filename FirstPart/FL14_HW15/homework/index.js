/* START TASK 1: Your code goes here */
const cell = document.querySelector('.cell');
const cell_1 = document.querySelector('.cell_1');
const special_cell = document.querySelector('.special_cell');

let color = ['yellow', 'blue', 'green'];

for (let i = 0; i < cell.length; i++) {
    cell.addEventListener("click", changeColor);
    cell_1.addEventListener("click", changeColor);
    special_cell.addEventListener("click", changeColor);

}

function changeColor(){
    // if (obj === cell){
    // cell.style.background = 'yellow';        
    // } else if (obj === cell_1) {
    //     cell_1.style.background = 'blue';        
    // } else if (obj === special_cell) {
    //     special_cell.style.background = 'green';        
    // } 

    for (let i = 0; i < color.length; i++) {
        cell.style.background = color[i];
        cell_1.style.background = color[i+1];
        special_cell.style.background = color[i+2];
    }
}

/* END TASK 1 */

/* START TASK 2: Your code goes here */

/* END TASK 2 */

/* START TASK 3: Your code goes here */

/* END TASK 3 */
