function visitLink(path) {
	let count = localStorage.getItem(path);
	if (count === null) {
	  count = 0;
	}
	count++;
	localStorage.setItem(path, count);
	console.log(count);
}

function viewResults() {
	for(let i=0; i<localStorage.length; i++) {
		let key = localStorage.key(i);
		var text = document.createTextNode(`You visited ${key} ${localStorage.getItem(key)} time(s)`);
		document.querySelector('#content').appendChild(text);
		document.querySelector('#content').appendChild(document.createElement('br'));

	}
}
