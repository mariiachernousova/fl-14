while(true){
    const input = prompt('Please, enter expression:');
    const expression = input.split(' ').join('');
    // alert(calculate(expression)); 
    try{
        const result = eval(expression);
        if (isNaN(result) || result === undefined) {
            alert('Invalid input');
        } else {
            alert(`${expression} = ${result}`);
        }
    } catch (error){
        alert('Invalid input');
    }
}

