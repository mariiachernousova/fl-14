let batteriesAmount = +prompt('Please, enter Amount of batteries:', '');
let percent = +prompt('Please, enter Defective rate:', '');
let hundred = 100;
let two = 2;

if (batteriesAmount<0 || percent<0 || percent>hundred) {
  alert('Invalid input data');
} else {
  let amount = batteriesAmount / hundred * percent;
  let totalSum = batteriesAmount - amount;
  batteriesAmount = Math.floor(batteriesAmount * hundred) / hundred;
  percent = Math.floor(percent * hundred) / hundred;
  amount = Math.floor(amount * hundred) / hundred;
  totalSum = Math.floor(totalSum * hundred) / hundred;

alert(`
Amount of batteries: ${batteriesAmount}
Defective rate: ${percent}%
Amount of defective batteries: ${amount}
Amount of working batteries: ${totalSum}
`)
}
