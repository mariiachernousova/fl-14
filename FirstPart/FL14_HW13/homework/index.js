// 1
function getAge(dob) {
    let now = new Date(); 
    let today = new Date(now.getFullYear(), now.getMonth(), now.getDate()); 
    let dobnow = new Date(today.getFullYear(), dob.getMonth(), dob.getDate()); 
    let age = today.getFullYear() - dob.getFullYear();
    if (today < dobnow) {
        age = age-1;
      }
    return age;
}

// 2
function getWeekDay(date) {
    date = date || new Date();
    let days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    let day = date.getDay();

    return days[day];
}

// 3
function getProgrammersDay(year) {
    let progarammersDay = 256;
    let date = new Date(year, 0, progarammersDay);

    return date;
}

// 5
function isValidIdentifier(variable){
    return !!variable.match(/^[^0-9\s][0-9a-zA-z_$]*$/g);
}
// 6
function capitalize(str){
    return str.replace(/(^|\s)\S/g, function(a) {
        return a.toUpperCase()
    });
}

// 7
function isValidAudioFile(name){
    return !!name.match(/^[A-Za-z]+(?:.flac|.mp3|.alac|.aac)$/);
}

// 8
function getHexadecimalColors(color){
    return color.match(/#([a-f0-9]{3}){1,2}\b/gi);
}

// 9
function isValidPassword(value) {
    return !!value.match(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/g);  
}

// 10
  function addThousandsSeparators(rez) {
    let outrez = (rez+'').replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1,');
    return outrez;
  }