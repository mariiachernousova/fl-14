// isEquals function
let isEquals = (a,b) => {
return a===b;
}

// numberToString function
function numberToString(number){
    return number.toString();
}

// storeNames function
function storeNames(){
    return Array.from(arguments);
}

// getDivision function
function isBigger(number1, number2) {
    return number1 > number2;
}

function getDivision(number1, number2) {
  if (isBigger(number1, number2)) {
    return number1/number2;
  } else {
    return number2/number1;
  }
}

// negativeCount function
  function negativeCount(array) {
    let counter = 0;
    array.forEach(function(current) {
      if (current < 0){
        counter++;
      } else {
        counter;
      }
    });
    return counter;
  }

  // letterCount function
  function letterCount(str, letter){
    let string = str.toLowerCase();
     let counter = 0;
     for (let position = 0; position < str.length; position++){
        if (string.charAt(position) === letter){
          counter += 1;
          }
      }
      return counter;
    }

    // countPoints function
    let maxPoint = 3;
    function countPoints(array) {
        let counter = 0;
        let a = 0;
        let b = 0;
        array.forEach(function(item) {
            a = Number(item.split(':')[0]);
            b = Number(item.split(':')[1]);
            if ( a === b) {
                counter += 1;
            } else if (isBigger(a, b)) {
                counter += maxPoint;
            }
        });
        return counter;
    }  
