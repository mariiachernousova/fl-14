window.addEventListener('load', function () {
    getInitialUsers();
  });
  
  function getInitialUsers() {
    fetch('https://roman4ak.github.io/fe-oop-lab/mocks/epms.json').then(function (
      response
    ) {
      if (response.ok) {
        response.json().then(function (json) {
          buildEmployeeTree(json);
        });
      } else {
        document.getElementsByClassName(
          'root'
        )[0].innerHTML = `Network request for employee.json failed with response ${response.status}`;
      }
    });
  }
let root = document.getElementsByClassName('root')[0];
let nav = document.querySelector('nav');
nav.addEventListener('click', menuBarHandler);
let employeesPage = document.querySelector('[href*="employeeTree"]');
let unitsPage = document.querySelector('[href*="unitsTree"]');
let warningPage = document.querySelector('[href*="warningTree"]');
let pageHeader = document.querySelector('h2');
let allCompanyEmployees;
const colorPalete = [
  '#A52A2A',
  '#CD853F',
  '#00FFFF',
  '#FFFDD0',
  '#FFC0CB',
  '#008000',
  '#FDEAA8',
  '#FFC0CB',
  '#E0FFFF',
  '#FFF44F',
  '#FDF4E3'
];

let Employee = function (
  id,
  rm_id,
  name,
  performance,
  last_vacation_date = 'null',
  salary,
  pool_name
) {
  this.children = [];
  this.id = id;
  this.rm_id = rm_id;
  this.name = name;
  this.performance = performance;
  this.last_vacation_date = last_vacation_date;
  this.salary = salary;
  this.pool_name = pool_name || null;
};

Employee.prototype = {
  add: function (child) {
    this.children.push(child);
  },

  remove: function (child) {
    let length = this.children.length;
    for (let i = 0; i < length; i++) {
      if (this.children[i] === child) {
        this.children.splice(i, 1);
        return;
      }
    }
  },

  getChild: function (i) {
    return this.children[i];
  },

  hasChildren: function () {
    return this.children.length > 0;
  }
};

let WarningCriterion = function () {
  this.criterion = '';
};

WarningCriterion.prototype = {
  setStrategy: function (criterion) {
    this.criterion = criterion;
  },

  chooseEmployees: function (employee, averageUnitSalary) {
    return this.criterion.chooseEmployees(employee, averageUnitSalary);
  }
};

let HIGH_SALARY = function () {
  this.chooseEmployees = this.chooseEmployees = function (
    employeeNode,
    averageUnitSalary
  ) {
    return employeeNode.salary > averageUnitSalary;
  };
};

let LOW_PERFOMANCE = function () {
  this.chooseEmployees = this.chooseEmployees = function (employeeNode) {
    return employeeNode.performance === 'low';
  };
};

let warningCriterion = new WarningCriterion();
let highSalary = new HIGH_SALARY();
warningCriterion.setStrategy(highSalary);
let twoWarningCriterion = new WarningCriterion();
let lowPerfomance = new LOW_PERFOMANCE();
twoWarningCriterion.setStrategy(lowPerfomance);

function menuBarHandler(event) {
  event.preventDefault();
  if (event.target.contains(employeesPage)) {
    createTree(root, allCompanyEmployees[0]);
  } else if (event.target.contains(unitsPage)) {
    createUnitsTree(root, allCompanyEmployees[0]);
  } else if (event.target.contains(warningPage)) {
    createWarningTree(root, allCompanyEmployees[0]);
  }
}

let iForColors;
function buildEmployeeTree(employeeData) {
  let employeeArray = employeeData.map((employee) => {
    return new Employee(
      employee.id,
      employee.rm_id,
      employee.name,
      employee.performance,
      employee.last_vacation_date,
      employee.salary,
      employee.pool_name
    );
  });

  employeeArray.forEach((employee) => {
    employeeArray.forEach((rm) => {
      if (employee.id === rm.rm_id) {
        employee.add(rm);
      }
    });
  });

  allCompanyEmployees = employeeArray;
  createTree(root, employeeArray[0]);
}

function createTree(container, obj) {
    if (Object.entries(obj).length === 0 && obj.constructor === Object) {
      document.getElementsByClassName('root')[0].innerHTML =
        'The organization has no employees.';
      return;
    }
    pageHeader.innerHTML = 'Employees Tree';
    let colorPaleteLength = colorPalete.length;
    iForColors = colorPaleteLength - 1;
    container.innerHTML = createTreeText(obj);
  }
  
  function createTreeText(node) {
    let li = '';
    let ul;
    li += `<li>`;
    if (node.hasChildren()) {
      li += `<strong>${node.name}</strong><ul>`;
      for (let i = 0, len = node.children.length; i < len; i++) {
        li += createTreeText(node.getChild(i));
      }
      li += `</ul></li>`;
  
      if (li) {
        ul = `<ul class="ulLevelItem" style="background-color: ${colorPalete[iForColors]};">${li}</ul>`;
      }
    } else {
      li += `${node.name}</li>`;
      return li;
    }
    iForColors -= 1;
    return ul || '';
  }
  function createUnitsTree(container, obj) {
    if (Object.entries(obj).length === 0 && obj.constructor === Object) {
      document.getElementsByClassName('Unitroot')[0].innerHTML =
        'There are no departments in the organization.';
      return;
    }
    pageHeader.innerHTML = 'Units Tree';
    let colorPaleteLength = colorPalete.length;
    iForColors = colorPaleteLength - 1;
    container.innerHTML = createUnitsTreeText(obj);
  }
  
  function createUnitsTreeText(node) {
    let li = '';
    let ul;
    li += `<li>`;
    if (node.pool_name) {
      li += `<strong>${node.pool_name}</strong>`;
  
      let unitAmount = node.children.length + 1;
      const averagePerfomanceData = getAverageUnitPerfomance(node);
      const averageUnitSalary = getAverageUnitSalary(node, unitAmount);
  
      li += `<p>High perfomance: ${averagePerfomanceData.averageHighPerfomance} from ${unitAmount} empoyees.
      Average perfomance: ${averagePerfomanceData.averageAveragePerfomance} from ${unitAmount} empoyees.
      Low perfomance: ${averagePerfomanceData.averageLowPerfomance} from ${unitAmount} unitAmount.
      Unit average perfomance is <b>${averagePerfomanceData.unitAveragePerfomance}</b>.</p>`;
      li += `<p>Average unit salary is <b>${averageUnitSalary}$</b>.</p></li>`;
  
      for (let i = 0, len = node.children.length; i < len; i++) {
        li += createUnitsTreeText(node.getChild(i));
      }
  
      ul = `<ul class="unitItem" style="background-color: ${colorPalete[iForColors]};">${li}</ul>`;
      iForColors -= 1;
    }
    return ul || '';
  }
  function createWarningTree(container, obj) {
    if (Object.entries(obj).length === 0 && obj.constructor === Object) {
      document.getElementsByClassName('Unitroot')[0].innerHTML =
        'The organization has no employees matching these filters.';
      return;
    }
    pageHeader.innerHTML = 'Warning Employees Tree';
    let colorPaleteLength = colorPalete.length;
    iForColors = colorPaleteLength - 1;
    container.innerHTML =
      '<p class="summary">Employees with low perfomance and too high salary:</p>' +
      createWarningTreeText(obj);
  }
  
  function createWarningTreeText(node, averageSalary, averagePerfomanceData) {
    let li = '';
    let ul;
    if (node.hasChildren()) {
      li += `<p><strong>${node.pool_name}</strong></p>`;
      averageSalary = getAverageUnitSalary(node, node.children.length + 1);
      averagePerfomanceData = getAverageUnitPerfomance(node);
      if (
        warningCriterion.chooseEmployees(node, averageSalary) &&
        twoWarningCriterion.chooseEmployees(node)
      ) {
        li += `<li><strong>${node.name}</strong></li>`;
      }
      for (let i = 0, len = node.children.length; i < len; i++) {
        li += createWarningTreeText(node.getChild(i), averageSalary, averagePerfomanceData);
      }
      ul = `<ul class="warningUlItem" style="background-color: ${colorPalete[iForColors]};">${li}</ul>`;
    } else {
      if (
        warningCriterion.chooseEmployees(node, averageSalary) &&
        twoWarningCriterion.chooseEmployees(node)
      ) {
        li += `<li>${node.name}</li>`;
      }
      return li;
    }
    iForColors -= 1;
    return ul || '';
  }
  const performancePalete = {
    low: -1,
    average: 0,
    top: 1,
    '-1': 'low',
    '0': 'average',
    '1': 'top'
  };
  
  function getAverageUnitPerfomance(node) {
    let averageLowPerfomance = node.children.reduce((acum, employee) => {
      if (performancePalete[employee.performance] === -1) {
        acum++;
      }
      return acum;
    }, 0);
    let averageAveragePerfomance = node.children.reduce((acum, employee) => {
      if (performancePalete[employee.performance] === 0) {
        acum++;
      }
      return acum;
    }, 0);
    let averageHighPerfomance = node.children.reduce((acum, employee) => {
      if (performancePalete[employee.performance] === 1) {
        acum++;
      }
      return acum;
    }, 0);
    if (node.performance === 'average') {
      averageAveragePerfomance++;
    } else if (node.performance === 'low') {
      averageLowPerfomance++;
    } else {
      averageHighPerfomance++;
    }
    let allPerfomances = [
      averageAveragePerfomance,
      averageLowPerfomance,
      averageHighPerfomance
    ];
    let unitAveragePerfomance;
    let maxQuantityPerfomance = Math.max.apply(null, allPerfomances);
    if (maxQuantityPerfomance === allPerfomances[0]) {
      unitAveragePerfomance = performancePalete[0];
    } else if (maxQuantityPerfomance === allPerfomances[1]) {
      unitAveragePerfomance = performancePalete[-1];
    } else {
      unitAveragePerfomance = performancePalete[1];
    }
    const averagePerfomanceData = {
      averageHighPerfomance,
      averageAveragePerfomance,
      averageLowPerfomance,
      unitAveragePerfomance
    };
    return averagePerfomanceData;
  }
  function getAverageUnitSalary(node, unitAmount) {
    let nodeChildrenSalary = node.children.reduce((amount, employee) => {
      amount += employee.salary;
      return amount;
    }, 0);
    let allSalary = nodeChildrenSalary + node.salary;
    let averageUnitSalary = allSalary / unitAmount;
    return averageUnitSalary.toFixed(2);
  }